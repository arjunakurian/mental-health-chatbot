import pandas as pd
from flask import Flask, render_template, request, redirect, url_for
import openai

# Set up OpenAI API key
openai.api_key = "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"

# Load dataset from csv file
dataset = pd.read_csv("dataset.csv")

# Define function to prompt user and get response
def ask_question(prompt):
    response = input(prompt + " ")
    return response.lower()

# Define function to generate advice based on user input
def generate_advice(problem):
    # Construct prompt for OpenAI API
    prompt = f"I am feeling {problem}. Please provide some advice."
    # Use OpenAI's GPT-3 model to generate advice based on the prompt
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt=prompt,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.7
    )
    # Extract the generated advice from the API response
    advice = response.choices[0].text.strip()
    return advice

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/advice', methods=['POST'])
def advice():
    age = int(request.form['age'])
    return redirect(url_for('show_questions', age=age))

@app.route('/questions/<age>', methods=['GET', 'POST'])
def show_questions(age):
    age = int(age)  # Convert age to integer
    
    if request.method == 'GET':
        if age >= 13 and age <= 17:
            questions = dataset["Questions for 13-17"].tolist()
        elif age >= 18 and age <= 21:
            questions = dataset["Questions for 18-21"].tolist()
        elif age >= 21 and age <= 25:
            questions = dataset["Questions for 21-25"].tolist()
        elif age >= 25 and age <= 40:
            questions = dataset["Questions for 25-40"].tolist()
        elif age >= 40 and age <= 60:
            questions = dataset["Questions for 40-60"].tolist()
        else:
            questions = dataset["Questions for 60+"].tolist()
        
        return render_template('questions.html', age=age, questions=questions)
    
    elif request.method == 'POST':
        answers = []
        for i in range(6):
            answer = request.form.get(f'answer{i+1}')
            answers.append(answer)
        # Process the answers and generate advice
        advice = generate_advice(answers)
        return render_template('advice.html', advice=advice)

if __name__ == '__main__':
    app.run(debug=True)
